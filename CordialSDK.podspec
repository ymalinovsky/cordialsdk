Pod::Spec.new do |spec|

  spec.name          = "CordialSDK"
  spec.version       = "0.4.1"
  spec.summary       = "CordialSDK"

  spec.description   = <<-DESC
    The Cordial SDK allows application developers to pass customer and event data from their applications to the Cordial platform.
  DESC

  spec.homepage      = "https://gitlab.com/cordialinc/mobile-sdk/ios-sdk"

  spec.license       = { :type => "MIT", :file => "LICENSE" }

  spec.author        = "Cordial Experience, Inc."

  spec.ios.deployment_target = '10.0'

  spec.ios.vendored_frameworks = 'CordialSDK.framework'

  spec.source = { :http => 'https://bitbucket.org/ymalinovsky/cordialsdk/raw/e508c922e4d7ef39d2b569d012c93d8f76c43a9b/CordialSDK.zip' }

  spec.exclude_files = "Classes/Exclude"

end
