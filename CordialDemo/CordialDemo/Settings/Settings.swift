//
//  Settings.swift
//  CordialDemo
//
//  Created by Yan Malinovsky on 08.10.2019.
//  Copyright © 2019 cordial.io. All rights reserved.
//

import Foundation

enum Settings: String {
    case qc = "QC"
    case staging = "Staging"
    case production = "Production"
    case custom = "Custom"
}
