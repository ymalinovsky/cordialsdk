//
//  CordialNotificationServiceExtension.h
//  CordialAppExtensions_Objective-C
//
//  Created by Yan Malinovsky on 7/30/19.
//

#import <UserNotifications/UserNotifications.h>

@interface CordialNotificationServiceExtension : UNNotificationServiceExtension

@end
