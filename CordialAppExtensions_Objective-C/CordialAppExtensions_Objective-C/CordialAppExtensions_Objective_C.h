//
//  CordialAppExtensions_Objective_C.h
//  CordialAppExtensions_Objective-C
//
//  Created by Yan Malinovsky on 7/30/19.
//

#import <UIKit/UIKit.h>

//! Project version number for CordialAppExtensions_Objective_C.
FOUNDATION_EXPORT double CordialAppExtensions_Objective_CVersionNumber;

//! Project version string for CordialAppExtensions_Objective_C.
FOUNDATION_EXPORT const unsigned char CordialAppExtensions_Objective_CVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CordialAppExtensions_Objective_C/PublicHeader.h>

#import <CordialAppExtensions_Objective_C/CordialNotificationServiceExtension.h>
